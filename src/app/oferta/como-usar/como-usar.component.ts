import { OfertasService } from './../../ofertas.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-como-usar',
  templateUrl: './como-usar.component.html',
  styleUrls: ['./como-usar.component.css']
})
export class ComoUsarComponent implements OnInit {

  public comoUsar: string;

  constructor(private router: ActivatedRoute, private ofertaService: OfertasService) { }

  ngOnInit() {

    this.ofertaService.getComoUsarOfertaByid(this.router.parent.snapshot.params['id'])
    .then(comoUsar => this.comoUsar = comoUsar);

  }

}

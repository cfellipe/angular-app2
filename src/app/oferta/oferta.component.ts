import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { OfertasService } from './../ofertas.service';
import { Oferta } from './../shared/oferta.model';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-oferta',
  templateUrl: './oferta.component.html',
  styleUrls: ['./oferta.component.css']
})
export class OfertaComponent implements OnInit {

  public oferta: Oferta;

  constructor(private route: ActivatedRoute, private ofertaService: OfertasService, private loadingService: Ng4LoadingSpinnerService) {

   }

  ngOnInit() {
    this.ofertaService.getOfertasById(this.route.snapshot.params['id'])
    .then(oferta => {
      this.loadingService.show();
     setTimeout(() => {
       this.oferta = oferta;
       this.loadingService.hide();
     }, 2000);
    });

  }

}

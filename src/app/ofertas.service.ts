import { environment } from './../environments/environment';
import { Http } from '@angular/http';
import { Oferta } from './shared/oferta.model';
import { Injectable } from '@angular/core';
import { PromiseState } from '../../node_modules/@types/q';
@Injectable()
export class OfertasService {

    constructor(private http: Http) {}


    public getOfertas(): Promise<Array<Oferta>> {
        return this.http.get(`${environment.api}/ofertas`)
        .toPromise()
        .then(ofertas => ofertas.json());
    }

    public getOfertasRestaurante(categoria: string): Promise<Oferta[]> {
        return this.http.get(`${environment.api}/ofertas?categoria=${categoria}`)
        .toPromise()
        .then(oferta => oferta.json());
    }

    public getOfertasDiversao(categoria: string): Promise<Oferta[]> {
        return this.http.get(`${environment.api}/ofertas?categoria=${categoria}`)
        .toPromise()
        .then(oferta => oferta.json());
    }

    public getOfertasById(id: number): Promise<Oferta> {
        return this.http.get(`${environment.api}/ofertas?id=${id}`)
        .toPromise()
        .then(oferta => oferta.json().shift());
    }

    public getComoUsarOfertaByid(id: number): Promise<string> {
        return this.http.get(`${environment.api}/como-usar?id=${id}`)
        .toPromise()
        .then(comoUsar => comoUsar.json()[0].descricao);
    }

    public getOndeFicaOfertaByid(id: number): Promise<string> {
        return this.http.get(`${environment.api}/onde-fica?id=${id}`)
        .toPromise()
        .then(ondeFica => ondeFica.json()[0].descricao);
    }
   /* public getOfertas(): Promise<Array<Oferta>> {

        // tslint:disable-next-line:no-shadowed-variable
        return new Promise((resolve, reject) => {
            setTimeout(() => resolve(this.ofertas), 3000);
            reject({ codigo: 404, mensagemErro: 'Servidor não encontrado' });
        });
    }*/
}

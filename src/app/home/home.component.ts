import { Oferta } from './../shared/oferta.model';
import { Component, OnInit } from '@angular/core';
import { OfertasService } from '../ofertas.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public ofertas: Array<Oferta>;

  constructor( private ofertaService: OfertasService, private loadingService: Ng4LoadingSpinnerService) { }

  ngOnInit() {

   this.ofertaService.getOfertas()
   .then((oferta) => {
    this.loadingService.show();
     setTimeout(() => {
      this.ofertas = oferta;
      this.loadingService.hide();
     }, 2000);
   })
   .catch((erro) => {
     console.log(erro);
   });

  }

}
